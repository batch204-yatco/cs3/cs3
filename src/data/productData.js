const productsData = [
        {
            id: "803.542.76",
            name: "MICKE",
            dimensions: "105x50 cm (41 3/8x19 5/8in)",
            description: "A clean and simple look that fits just about anywhere. You can combine it with other desks or drawer units in the MICKE series to extend your work space. The clever design at the back hides messy cables.",
            price: 6990,
            onOffer: true
        },
        {
            id: "794.295.79",
            name: "TROTTEN",
            dimensions: "120x70 cm (47 1/4x27 1/2 )",
            description: "Changing positions from sitting to standing is good for you, and the crank handle allows you to work your arms while adjusting the height. Moving your body makes you both feel and work better.",
            price: 10890,
            onOffer: true
        },
        {
            id: "805.251.17",
            name: "OBEGRÄNSAD",
            dimensions: "160x75 cm (63x29 12in)",
            description: "A dream for those who love to create music. This desk is designed to hold everything you need. Created in collaboration with the pros at Swedish House Mafia and part of the OBEGRÄNSAD collection.",
            price: 7990,
            onOffer: true
        }
    ]

export default productsData;
