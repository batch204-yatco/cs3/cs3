import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';

export default function Login(props) {
	
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	const { user, setUser } = useContext(UserContext);

	useEffect(() => {
		// console.log(email)
		// console.log(password1)
		// console.log(password2)

		// if all fields are populated and passwords match
		if(email !== '' && password !== '') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	const retrieveUserDetails = (token) => {
		console.log(token)
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data._id)//null
			//set our user state to include the user's id and isAdmin values
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			console.log(data.isAdmin)//null
		})
		
	}

	function authenticateUser(e){
		e.preventDefault()//prevent default form behavior so form does not submit
		console.log(e)
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			/*if email exists*/
			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access)
				//call the retrieveUserDetails function and pass the JWT to it
				retrieveUserDetails(data.access)

				alert("Successfully logged in.")
				// localStorage.setItem('email', email)

				// setUser({
				// 	email: email
				// })
				console.log(email)//daph@mail.com
				props.history.push("/products");
			}else{
				alert("Login failed. Please try again");
				setEmail("");
				setPassword("");
				
			}
		})
			
	}

	return (
			(user.id !== null) ?
			<Redirect to="/"/>
			:
			<Form onSubmit={e => authenticateUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>


			{/*If filled out, button can be clicked*/}
			{
				isActive ? 	
					<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
					Login
					</Button>
					:
					<Button className="mt-3" variant="primary" id="submitBtn" disabled>
					Login
					</Button>	
			}
	
		</Form>
	)

}


