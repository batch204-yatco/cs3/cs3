import Banner from '../components/Banner';


export default function Home(){

	const data = {
		title: "IKEA",
		content: "Affordable Swedish design made for everyday Filipino life",
		destination: "/products",
		label: "Products"
	}


	return(
		<>
			<Banner dataProp={data}/>
		</>
	)
}
