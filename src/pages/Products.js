import { useEffect, useState, useContext } from 'react';
// import productData from '../data/productData';
import ProductCard from '../components/ProductCard';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext';


export default function Products(){

	const [productsData, setProductsData] = useState([])

	const { user } = useContext(UserContext)

	// Check if mock data was captured
	// console.log(productData);
	// console.log(productData[0]);

	// Props
		// Shorthand for "property" since components are considered as object in ReactJS
		// Way to pass data from parent component to child component
		// Synonymous to the function parameter
		// Referred to as "props drilling"
		//productProp is user defined

	// Fetch by default always makes aa GET request, unless a different one is specified
	// ALWAYS add fetch requests for getting data in a useEffect hook

		// console.log(process.env.REACT_APP_API_URL)
		// changes to env files are applied ONLY at build time when starting the project locally

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProductsData(data)
		})
	}
	useEffect(() => {
		// console.log(process.env.REACT_APP_API_URL)
		// changes to env files are applied ONLY at build time (when starting the project locally)

		fetchData()

	}, [])


	const products = productsData.map(product => {
		if(product.isActive){
			return (
				<ProductCard productProp={product} key={product._id}/>
			)
		}else{
			return null
		}
	})
	console.log(user.isAdmin)
	return (
		(user.isAdmin) ?
		<AdminView productsProp={productsData} fetchData={fetchData}/>
		:
		<>
			<h1>Products</h1>
			{products}
		</>
	)
}
