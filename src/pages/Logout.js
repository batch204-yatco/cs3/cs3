import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	// get the setUser setter from App.js
	const { setUser } = useContext(UserContext);
	const { setItemsArr } = useContext(UserContext);
	

	// when component mounts/page loads, set the user state to null
	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		})
		setItemsArr({
			id: null
		})
		
	}, [])

	localStorage.clear()
	return(
		// redirects user to login page
		<Redirect to="/login"/>
	)
}
