import { useState, useEffect, useContext } from 'react';
import { Card, Table, Button, Modal, Form } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import SpecificProduct from './SpecificProduct'
// import { get } from 'cart-localstorage'
import UserContext from '../UserContext';

export default function Cart(){

const cartFromLocalStorage = JSON.parse(localStorage.getItem('__cart') || '[]')
const [ cart, setCart ] = useState(cartFromLocalStorage)
const [name, setName] = useState("")
const [price, setPrice] = useState(0)
const [quantity, setQuantity] = useState(0)
const [subtotal, setSubtotal] = useState (0)
const [itemsArr, setItemsArr] = useState([])
// var totalCartPrice = 0
const [totalCartPrice, setTotalCartPrice] = useState(0)

const { user } = useContext(UserContext)

const subtractOrder = (itemToSubtract) => {
	setCart(cart => 
		cart.map((item)=>
			itemToSubtract === item.id ? {...item, quantity: item.quantity - (item.quantity >1 ? 1 : 0)} : item
		)
	)
	
	// const priceToSubArr = cart.map((item)=>{

	// 	if (item.id === itemToSubtract){
	// 		return item.price
	// 	} else {
	// 		return 0
	// 	}
	// 	// return  itemToSubtract === item.id ?item.price ;

	// })
	// let arrToReduce = priceToSubArr
	// let sum = arrToReduce.reduce(function (a,b){
	// 	return a+b;
	// 	}, 0)
	// console.log(totalCartPrice)
	// console.log(sum)
	// // setTotalCartPrice(sum)
	// setTotalCartPrice(totalCartPrice-sum)
	
}

const addOrder = (itemToAdd) => {
	setCart(cart => 
		cart.map((item)=>
			itemToAdd === item.id ? {...item, quantity: item.quantity + 1} : item
		)
	)

	// const priceToAddArr = cart.map((item)=>{

	// 	if (item.id === itemToAdd){
	// 		return item.price
	// 	} else {
	// 		return 0
	// 	}

	// })

	// let arrToReduce = priceToAddArr
	// let sum = arrToReduce.reduce(function (a,b){
	// 	return a+b;
	// 	}, 0)
	// console.log(sum)
	// // setTotalCartPrice(sum)
	// setTotalCartPrice(totalCartPrice+sum)
}


// Remove Order Function
const removeOrder = (itemToRemove) => {

	setCart(cart => 
		cart.filter(item => item.id !== itemToRemove)	
	)

}

const checkout = () => {
	alert("Order Checkout Successful!")
	setCart([])
}

useEffect(() => {
	//map through the cart to generate table contents
	const items = cart.map(item => {

		// const tcp = totalCartPrice += 3;
		// setTotalCartPrice(tcp)
		// console.log(tcp)
		
		return(
			<tr key={item._id}>
				<td>{item.name}</td>
				<td>{item.price}</td>
				<td>
					<Button variant="secondary" size="sm" onClick={() => subtractOrder(item.id)}>-
					</Button>
					{item.quantity}
					<Button variant="secondary" size="sm" onClick={() => addOrder(item.id)}>+
					</Button>
				</td>
				<td>{item.price * item.quantity}</td>
				<td><Button variant="warning" size="sm" onClick={() => removeOrder(item.id)}>Remove</Button></td>
			</tr>
		)
	})
	
	setItemsArr(items)
}, [cart])

useEffect(() => {
const tcp = cart.map(item => {
	setTotalCartPrice(0)
	const eachSub = item.price * item.quantity
	console.log(eachSub)

	return (eachSub)
	// setTotalCartPrice(eachSub)
	})
	
	let ttcp = tcp
	let sum = ttcp.reduce(function (a,b){
		return a+b;
	}, 0)
	console.log(sum)
	setTotalCartPrice(sum)


}, [cart])

 return (

 	(user.isAdmin || user.id === null) ?
		<Redirect to="/"/>
		:
       <Table striped bordered hover responsive>
		<thead className="bg-dark text-white">
			<tr>
				<th>Name</th>
				<th>Price</th>
				<th>Quantity</th>
				<th>Subtotal</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{itemsArr}
		</tbody>
		
			<tr>
			<Button variant="success" size="sm" onClick={checkout}>Checkout</Button>
			<td></td>
			<td></td>
			<td>TOTAL: {totalCartPrice}</td>
			</tr>
		
	</Table>
  );
}

