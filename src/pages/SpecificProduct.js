import { useState, useEffect, useContext } from 'react';
import { Container, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Cart from '../pages/Cart'
import { add, get } from 'cart-localstorage'
import UserContext from '../UserContext';

const cartFromLocalStorage = JSON.parse(localStorage.getItem('cart') || '[]')

export default function SpecificProduct(props) {

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	//match.params holds the ID of our product in the productId property
	const productId = props.match.params.productId;
	const [quantity, setQuantity] = useState(0);

	const [cart, setCart] = useState(cartFromLocalStorage)
	console.log(cart)
	const token = localStorage.getItem("token")
	const { user } = useContext(UserContext)
	const [newCart, setNewCart] = useState([])
	

	const addProduct = () => {
		const myproduct = {id: productId, name: name, price: price, quantity: 1 }
		add(myproduct, quantity)
		alert ("Product added to cart!")
		const newProd = get(productId)
		console.log(get(productId))
		console.log(newProd)
		localStorage.setItem('cart', JSON.stringify(newProd))
		console.log(cartFromLocalStorage)
		setNewCart(newProd)
		console.log(newCart)
	}

		

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [cart])

	useEffect(() => {
		localStorage.setItem("cart", JSON.stringify(cart))
		
	}, [cart])

	return(
		<Container className="mt-5">
			<Card>
				<Card.Body className="text-center">
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>

					{/*<Form.Group controlId="Quantity">
					<Form.Label>Quantity</Form.Label>
					<Form.Control
						type="quantity"
						placeholder="0"
						value={quantity}
						onChange={e => setQuantity(e.target.value)}
						required
					/>
					</Form.Group>*/}
					{(user.isAdmin || user.id === null) ?
					<Link className="btn btn-primary" to={`/`}>Add to Cart</Link>
						:
					<Link className="btn btn-primary" onClick={()=>addProduct()} to={`/cart`}>Add to Cart</Link>
					}
				</Card.Body>
			</Card>
		</Container>

	)
}


