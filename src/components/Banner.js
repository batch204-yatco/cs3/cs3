import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function Banner({dataProp}){

	const {title, content, destination, label} = dataProp

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>Welcome to IKEA!</h1>
				<p>Affordable Swedish design made for everyday Filipino life</p>
				<Link className="btn btn-primary" to={destination}>{label}</Link>
			</Col>
		</Row>
	)
}
