import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// courseProp will capture courseProps in Courses.js
// Destructuring is done in the parameter to retrieve courseProp
export default function ProductCard({productProp}){

	// console.log(props.courseProp)
	// console.log(courseProp)

	const {_id, name, description, price} = productProp;

	// Use state hook for this component to be able to store its state
	// States are used to keep track of info related to individual components
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue)

	// const [count, setCount] = useState(0);


	// count is to view value in useState, setCount is to change it

	// Using state hook returns an array with the first element being a value and second element as a function that's used to change the value of first element
	// console.log(useState(0));


	// Function that keeps track of enrollees for a course
	// By default JavaScript is synchronous as it executes code from the top of the file to the bottom and will wait for the completion of one expression before it proceeds to the next
	// Setter function for useStates are asynchronous allowing it to execute separately from other codes in the program. Changes value.
	// Getter is to view value
	// setCount function is being executed while console.log is already being completed resulting in the console to be behind by one count

	// function details (){

	// 	// if (count < 30){
	// 	// 	setCount(count + 1);
	// 	// 	console.log('Enrollees: ' + count)
	// 	// } else {
	// 	// 	setCount (count)
	// 	// 	console.log ('No more seats')
	// 	// }
	// }
	// console.log is delayed b/c setCount executes faster 

	return(
		<Card className="mb-2">
		    <Card.Body>
		        <Card.Title>
		            {name}
		        </Card.Title>
		        <Card.Subtitle>
		            Description:
		        </Card.Subtitle>
		        <Card.Text>
		            {description}
		        </Card.Text>
		        <Card.Subtitle>
		            Price:
		        </Card.Subtitle>
		        <Card.Text>
		            PhP {price}
		        </Card.Text>
		     
		        <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
		    </Card.Body>
		</Card>
	)
}
