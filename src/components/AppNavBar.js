
import { useContext } from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavBar(){
	const { user } = useContext(UserContext);

		console.log(user.id)
	return(
		<Navbar bg="light" expand="lg">
		  <Container>
		    <Link className="navbar-brand" to="/">IKEA</Link>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
			{/*
				- className is use instead class, to specify a CSS class

				- changes for Bootstrap 5
				from mr -> to me
				from ml -> to ms
			*/}
		      <Nav className="ms-auto">
		        <Link className="nav-link" to="/">Home</Link>
		        <Link className="nav-link" to="/products" exact>Products</Link>

		        {(user.id !== null) ?
		        	<>
		        	<Link className="nav-link" to="/logout">Logout</Link>
		        	{(user.isAdmin === false) ? 
		        		<Link className="nav-link" to="/cart">Cart</Link>
			        	:
			        	<Link className="nav-link" to="/"></Link>
		        	}
			        </>
		        	:
		        	<>
			        	<Link className="nav-link" to="/login">Login</Link>
			        	<Link className="nav-link" to="/register">Register</Link>
		        	</>
		        }

		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}
